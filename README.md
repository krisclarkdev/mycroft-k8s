# About

I have a 12 node [Kubernetes](https://kubernetes.io/) cluster running in my garage and I've aways wanted [mycroft](https://github.com/MycroftAI/mycroft-core) out here.  Which got me thinking.  How can I run it on [Kubernetes](https://kubernetes.io/).  This manifest is running in my lab but I've yet to test any hardware with it.  YMMV

# Supported CPUs

I've only tested this on my x86 nodes and then cross compiled for arm64 with docker.  I'd assume I need to rebuild it for arm64 so I've no idea if it works or not.

# Download the default config files

```
curl https://github.com/MycroftAI/mycroft-core/blob/dev/mycroft/configuration/mycroft.conf -o mycroft-core.conf
```

# Covert the config files to [base64](https://www.base64decode.org/)

cat mycroft-core.conf | base64  > mycroft-core.txt

# Modify the [configmap](https://kubernetes.io/docs/concepts/configuration/configmap/)

Update mycroft.yaml with the values from above

```
apiVersion: v1
kind: Secret
metadata:
  name: "mycroft-config"
  namespace: "mycroft"
type: Opaque
data:
  mycroft-core.conf CONTENT OF MYCROFT-CORE.TXT
```

# Deploy

```
kubectl create -f ./mycroft.yaml
```

# Connect to [mycroft](https://github.com/MycroftAI/mycroft-core) remotely

### Get the service IP

```
kubectl get services -n mycroft
```

# Connecting remotely

I currently don't know if mycroft has any ways to connect to the CLI remotely.  I'm currently working on a webapp to talk with the mycroft cli
